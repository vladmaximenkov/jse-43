package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.api.repository.dto.ISessionDTORepository;
import ru.vmaksimenkov.tm.dto.SessionDTO;
import ru.vmaksimenkov.tm.marker.DBCategory;
import ru.vmaksimenkov.tm.repository.dto.SessionDTORepository;
import ru.vmaksimenkov.tm.repository.dto.UserDTORepository;

public class SessionRepositoryTest extends AbstractTest {

    @NotNull
    private static final ISessionDTORepository SESSION_REPOSITORY = new SessionDTORepository(ENTITY_MANAGER);

    @Nullable
    private static SessionDTO TEST_SESSION;

    @NotNull
    private static String TEST_SESSION_ID;

    @Test
    @Category(DBCategory.class)
    public void clear() {
        SESSION_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertTrue(SESSION_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        SESSION_REPOSITORY.clear();
        Assert.assertTrue(SESSION_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findByUserId() {
        Assert.assertFalse(SESSION_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        Assert.assertEquals(SESSION_REPOSITORY.findAll(TEST_USER_ID).get(0), TEST_SESSION);
    }

    @After
    public void finishTest() {
        new UserDTORepository(ENTITY_MANAGER).clear();
        SESSION_REPOSITORY.clear();
    }

    @Test
    @Category(DBCategory.class)
    public void removeByUserId() {
        SESSION_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertEquals(0, (long) SESSION_REPOSITORY.size(TEST_USER_ID));
    }

    @Before
    @Category(DBCategory.class)
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final SessionDTO session = new SessionDTO();
        TEST_SESSION_ID = session.getId();
        session.setUserId(TEST_USER_ID);
        SESSION_REPOSITORY.add(session);
        TEST_SESSION = SESSION_REPOSITORY.findById(TEST_SESSION_ID);
    }
}
