package ru.vmaksimenkov.tm.api.service.dto;

import ru.vmaksimenkov.tm.api.repository.dto.IAbstractDTORepository;
import ru.vmaksimenkov.tm.dto.AbstractEntityDTO;

public interface IAbstractDTOService<E extends AbstractEntityDTO> extends IAbstractDTORepository<E> {

}
