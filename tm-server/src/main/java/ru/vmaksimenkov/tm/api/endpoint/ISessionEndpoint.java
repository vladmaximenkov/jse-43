package ru.vmaksimenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.SessionDTO;
import ru.vmaksimenkov.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    boolean closeSession(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @Nullable
    @WebMethod
    UserDTO getUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @Nullable
    @WebMethod
    SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

}
