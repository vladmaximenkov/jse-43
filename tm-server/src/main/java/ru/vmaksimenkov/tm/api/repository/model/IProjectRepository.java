package ru.vmaksimenkov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.ProjectDTO;
import ru.vmaksimenkov.tm.model.Project;

public interface IProjectRepository extends IAbstractBusinessRepository<Project> {

    boolean existsByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project findByName(@Nullable String userId, @Nullable String name);

    @Nullable
    String getIdByName(@Nullable String userId, @Nullable String name);

    void removeByName(@Nullable String userId, @Nullable String name);

}
