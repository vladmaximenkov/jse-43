package ru.vmaksimenkov.tm.exception.entity;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Error! TaskDTO not found...");
    }

}
