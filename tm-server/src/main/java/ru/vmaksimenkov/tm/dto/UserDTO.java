package ru.vmaksimenkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "app_user")
@NoArgsConstructor
public class UserDTO extends AbstractEntityDTO {

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstName;

    @Column
    @Nullable
    private String lastName;

    @Column
    private boolean locked = false;

    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String middleName;

    @Column
    @Nullable
    private String passwordHash;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

}
