package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.endpoint.SessionDTO;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionDTO> {

    boolean contains(@NotNull String id);

    List<SessionDTO> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

}
