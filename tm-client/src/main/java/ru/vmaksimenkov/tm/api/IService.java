package ru.vmaksimenkov.tm.api;


import ru.vmaksimenkov.tm.endpoint.AbstractEntityDTO;

public interface IService<E extends AbstractEntityDTO> extends IRepository<E> {

}
