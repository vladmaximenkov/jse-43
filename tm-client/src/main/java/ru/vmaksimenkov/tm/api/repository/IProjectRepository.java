package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.endpoint.ProjectDTO;

public interface IProjectRepository extends IRepository<ProjectDTO> {

    boolean existsByName(@NotNull String userId, @NotNull String name);

    @Nullable
    ProjectDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    String getIdByName(@NotNull String userId, @NotNull String name);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @NotNull String name);

}
