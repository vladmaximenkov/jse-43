package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.bootstrap.Bootstrap;
import ru.vmaksimenkov.tm.endpoint.SessionDTO;
import ru.vmaksimenkov.tm.endpoint.SessionEndpoint;

public abstract class AbstractEndpointTest {

    @NotNull
    protected static final String ADMIN_USER_NAME = "admin";
    @NotNull
    protected static final String ADMIN_USER_PASSWORD = "admin";
    @NotNull
    protected static final Bootstrap BOOTSTRAP = new Bootstrap();
    @NotNull
    protected static final SessionEndpoint SESSION_ENDPOINT = BOOTSTRAP.getSessionEndpoint();
    @NotNull
    protected static final String TEST_USER_NAME = "test";
    @NotNull
    protected static final String TEST_USER_PASSWORD = "test";
    @NotNull
    protected static SessionDTO SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);

}
